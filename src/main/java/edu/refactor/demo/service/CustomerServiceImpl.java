package edu.refactor.demo.service;

import edu.refactor.demo.dao.BillingAccountDAO;
import edu.refactor.demo.dao.CustomerDAO;
import edu.refactor.demo.model.BillingAccount;
import edu.refactor.demo.model.Customer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
public class CustomerServiceImpl implements CustomerService {
    private static final Logger LOG = LoggerFactory.getLogger(CustomerServiceImpl.class);

    private final BillingAccountDAO billingAccountDAO;
    private final CustomerDAO customerDAO;

    @Autowired
    public CustomerServiceImpl(CustomerDAO customerDAO, BillingAccountDAO billingAccountDAO) {
        this.customerDAO = customerDAO;
        this.billingAccountDAO = billingAccountDAO;
    }

    @Override
    public List<Customer> getAllCustomers() {
        LOG.debug("Start getAllCustomers");

        return customerDAO.findCustomerByStatusNotLike("delete");
    }

    @Override
    @Transactional
    public List<BillingAccount> billingAccounts(Long id) {
        LOG.debug("Start billingAccounts for customer with id = {}", id);

        Optional<Customer> customer = customerDAO.findById(id);

        if (!customer.isPresent()) {
            LOG.error("Customer with id = {} was not found", id);
            throw new RuntimeException("Customer does not exist");
        }

        return customerDAO.findById(id).get().getBillingAccounts();
    }

    @Override
    public Customer createCustomer(Customer customer) {
        LOG.debug("Start createCustomer");

        if (customerDAO.findCustomerByLoginAndEmail(customer.getLogin(), customer.getEmail()) != null) {
            LOG.error("Customer with login = {} already exists", customer.getLogin());
            throw new RuntimeException("Create customer error");
        }
        Customer newCustomer = new Customer().copyFrom(customer);
        newCustomer = customerDAO.save(newCustomer);

        LOG.debug("New customer with id = {} was succussfully created", newCustomer.getId());

        BillingAccount billingAccount = new BillingAccount();
        billingAccount.setMoney(100);
        billingAccount.setPrimary(true);
        newCustomer.getBillingAccounts().add(billingAccount);
        billingAccount.setCustomer(newCustomer);
        billingAccountDAO.save(billingAccount);

        LOG.debug("Billing account for customer with id = {} was succussfully created", newCustomer.getId());

        return newCustomer;
    }

    @Override
    @Transactional
    public Customer getCustomerById(Long id) {
        LOG.debug("Start getCustomerById for customer with id = {}", id);

        Optional<Customer> customer = customerDAO.findById(id);

        if (!customer.isPresent()) {
            LOG.error("Customer with id = {} was not found", id);
            throw new RuntimeException("Customer does not exist");
        }

        return customerDAO.findById(id).get();
    }

    @Override
    @Transactional
    public BillingAccount billingAccountForCustomer(Long id, Long baId) {
        LOG.debug("Start billingAccountForCustomer for customer with id = {}, baId = {}", id, baId);

        Optional<Customer> customer = customerDAO.findById(id);

        if (!customer.isPresent()) {
            LOG.error("Customer with id = {} was not found", id);
            throw new RuntimeException("Customer does not exist");
        }

        return customer.get().getBillingAccounts().stream().filter(e -> e.getId().equals(baId)).findFirst().get();
    }

    @Override
    @Transactional
    public BillingAccount createBAForCustomer(BillingAccount billingAccount, Long id) {
        LOG.debug("Start createBAForCustomer for customer with id = {}", id);

        Customer customer = customerDAO.findById(id).get();
        billingAccount.setCustomer(customer);
        LOG.debug("Billing account for customer with id = {} was succussfully created", customer.getId());

        return billingAccountDAO.save(billingAccount);
    }
}
